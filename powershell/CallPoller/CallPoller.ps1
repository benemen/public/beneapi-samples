[CmdletBinding()]
param (
    # Credentials to authenticate to BeneAPI
    [Parameter()]
    [PSCredential] $Credential,
    # Polling interval in seconds
    [Parameter()]
    [Int] $PollingInterval = 3

)

############## Classes ##############

# Custom object for storing API related things
class BeneAPIInfo {
  # API Endpoint from discovery
  [String] $Endpoint
  # UserName of authenticated user
  [String] $UserName
  # UserId of authenticated user
  [String] $UserId
  # HTTP headers for API requests
  [hashtable] $Headers
}

# CallLeg status options from https://doc.enreachvoice.com/beneapi/#calllegstatus
enum CallStatus {
  InQueue = 1
  Allocated = 2	
  Connected = 3	
  Parked = 10
}

############## Functions ##############

<#
.SYNOPSIS
Retrieves API url and authenticate user to BeneAPI with username and password

.DESCRIPTION
Retrieves API url and authenticate user to BeneAPI with username and password. Returns BeneAPIInfo object if authentication succesfull

.PARAMETER Credential
PSCredential 

.EXAMPLE
$cred = Get-Credential "user.name@example.com"
AuthenticateBeneAPI -Credential $cred
#>

function AuthenticateBeneAPI {
  Param(
    [pscredential] $Credential
  )
  
  $ApiInfo = [BeneAPIInfo]::new()
  
  # Discovery 
  $apiEndpoint = (Invoke-RestMethod "https://discover.beneservices.com/api/user?user=$($Credential.UserName)").apiEndpoint
  if (-not $apiEndpoint) {
    Write-Warning "No API endpoint found for user $($Credential.UserName)"
    return
  }
  $ApiInfo.Endpoint = $apiEndpoint

  $authPayload = @{
    UserName = $Credential.UserName
    Password = $Credential.GetNetworkCredential().Password
  } | ConvertTo-Json -Compress

  $headers = @{
    #Authorization = "Basic $b64"
    Accept = "application/json"
  }

  $userAuth = Invoke-RestMethod "$apiEndpoint/authuser/$($Credential.UserName)/" -Method Post -ContentType "application/json" -Headers $headers -Body $authPayload

  if (-not $userAuth) {
    Write-Warning "API authentication failed"
    return
  }

  # Populate ApiInfo
  $ApiInfo.UserName = $userAuth.UserName
  $ApiInfo.UserId = $userAuth.UserID

  $b64 = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes("$($Credential.UserName):$($userAuth.SecretKey)"))
  $headers.Authorization = "Basic $b64"
  $ApiInfo.Headers = $headers

  return $ApiInfo
}

<#
.SYNOPSIS
Get active CallLegs for an auntehnticated user from BeneAPI

.DESCRIPTION
Get active calllegs for an auntehnticated user from BeneAPI.
https://doc.enreachvoice.com/beneapi/#get-callmanagement-legs

.PARAMETER Api
BeneAPIInfo object

#>
function GetActiveCalls {
  Param (
    [BeneAPIInfo] $Api
  )

  $url = "$($Api.Endpoint)/callmanagement/legs/?UserIds=$($Api.UserId)"
  Write-Verbose "Invoking GET [$url]"
  try {
    $callLegs = Invoke-RestMethod $url -Headers $Api.Headers -ErrorVariable errorVar 
    return $callLegs
  }
  catch {
    #if 404, no calls atm
    if (404 -eq [int]$errorVar.ErrorRecord.Exception.Response.StatusCode) {
      return
    }
    Write-Warning "Error`n$_"
  }
}

<#
.SYNOPSIS
Get Queues from API

.DESCRIPTION
Get Queues from API as a QueueId -> QueueName HashTable

.PARAMETER Api
BeneAPIInfo object

#>
function GetQueues {
  Param (
    [BeneAPIInfo] $Api
  )
  $url = "$($Api.Endpoint)/queues/"
  Write-Verbose "Invoking GET [$url]"

  try {
    $queuesHT = @{}
    $queues = Invoke-RestMethod $url -Headers $Api.Headers -ErrorVariable errorVar 

    foreach ($q in $queues) {
      $queuesHT[$q.Id] = $q.Name
    }
    return $queuesHT
  }
  catch {
    #if 404, no queues
    if (404 -eq [int]$errorVar.ErrorRecord.Exception.Response.StatusCode) {
      return
    }
    Write-Warning "Error`n$_"
  }
}

############## Process ##############

$legs = @{}

if (-not $Credential) {
  $Credential = Get-Credential -Message "Enter credentials for getting active call legs"
}

$apiInfo = AuthenticateBeneAPI $Credential

if (-not $apiInfo) {
  return 
}

Write-host "Authenticated to API [$($apiInfo.Endpoint)] as $($apiInfo.UserName)"

# Get queues
$queues = GetQueues -Api $apiInfo

# Start the loop
while ($true) {
  $callLegs = GetActiveCalls -Api $apiInfo
  if ($callLegs) {
    foreach ($c in $callLegs) {
      #Check if this leg + status has been displayed already
      $key = "$($c.Id)-$($c.CallLegStatusId)"
      if (-not $legs.ContainsKey($key)) {
        $legs[$key] = $c

        $queueName = $queues[$c.QueueId]
        $callStatus = [CallStatus] $c.CallLegStatusId
  
        # If queue not found by id, expecting that it's a direct call
        if ($queueName) {
          Write-host "`nService Call $callStatus"
          Write-host "CallId: $($c.CallId)"
          Write-host "Queue: $queueName"
          Write-host "Caller: $($c.SourceNumber)"
        }
        else {
          Write-host "`nDirect Call $callStatus"
          Write-host "CallId: $($c.CallId)"
          Write-host "Caller: $($c.SourceNumber)"
        }
      }
    }
  }
  else {
    Write-Verbose "No calls"
    $legs = @{}
  }

  Start-Sleep -seconds $PollingInterval
}





