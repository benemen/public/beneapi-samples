# CallPoller

An example powershell script for polling active CallLegs from API.

## How it works

First script discovers API endpoint address using [Discovery mechanism](https://doc.enreachvoice.com/beneapi/#service-discovery) and retrieves API SecretKey using Password authentication.

If API discovery and authentication is succesful, script starts polling active call legs from [Call management](https://doc.enreachvoice.com/beneapi/#call-management) endpoint for an authenticated user.

When there is new call legs/status, information will be printed out.

## Usage

``` PowerShell
PS> $Creds = Get-Credential user.name@example.com

PS> CallPoller.ps1 -Credential $Creds

Authenticated to API [https://api-theia.benedesk.com/] as user.name@example.com

Direct Call Allocated
CallId: 3d12ba28-9e72-4a90-bb29-57b8ac92d717
Caller: +358123456789

Direct Call Connected
CallId: 3d12ba28-9e72-4a90-bb29-57b8ac92d717
Caller: +358123456789

Service Call Allocated
CallId: 4b45d6ec-2859-4d10-8a19-fdaef87c9e57
Queue: TestQueue
Caller: +35899999999

Service Call Connected
CallId: 4b45d6ec-2859-4d10-8a19-fdaef87c9e57
Queue: TestQueue
Caller: +35899999999
```



