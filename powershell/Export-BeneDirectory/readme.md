# Export-BeneDirectory

Example PowerShell script for exporting BeneCloud directory from BeneAPI REST service. Results can be exported to CSV-file, Excel-file or as plain PSObjects.

## Things needed
- Using BeneAPI requres BeneAPI Account having permission to read directory
- Exporting to excel file requires ImportExcel module https://www.powershellgallery.com/packages/ImportExcel

## Parameters

| Parameter | Description |
|-----------|-------------|
|APIUsername|Username to access BeneAPI|
|APIKey|API secret key to access BeneAPI|
|EntryType | Which entrytypes should be exported. Options Users, DirectoryEntries, ServicePools, All. Default = All
|ExportType| How Directory entires should be exported. Options: CSV, Excel. If not given, return data as PSObject
|Directory | Name of directory to be exported. By default there is only one directory "Default", which contains all users, service pools and directorye entries |
| ExportPath | Optional filepath where export file will be saved. If not given, file is created on the current directory and filename is in format <directoryname>-directory-<date in format yyyy-MM-dd_HHmmss>.[csv|xlsx]  |
|NoMultiLines|Switch, should remove line endings from fields that allow multiline text, like description field.


## Examples

Export Directory information of all types to $directoryusers returns directory as PowerShell object

``` Powershell
$directoryusers = ./Export-BeneDirectory.ps1 -APIUsername api.user@example.com -APIKey secretkey
```

Export directory information of Users to Excel file to current folder
```
./Export-BeneDirectory.ps1 -APIUsername api.user@example.com -APIKey secretkey -EntryType Users -ExportType Excel
```    

Export directpry information of directory entries to excel file to C:\Exports\directory.xlsx
```
./Export-BeneDirectory.ps1 -APIUsername api.user@example.com -APIKey secretkey -ExportPath "C:\Exports\directory.xlsx" -ExportType Excel -EntryType DirectoryEntries
```    

### Update Active Directory information from BeneCloud Directory

``` Powershell
# Get directory from Benemen BeneAPI
$apiUser = "api.user@example.com"
$apiKey = "xxYYzzKK123123xyzXYZ"
$directory = ./Export-BeneDirectory -APIUsername $apiUser -APIKey $apiKey 

# Loop through retrieved directory information, and update AD if mobilephone is diferent
foreach ($d in $directory) {
  # search AD user, expecting that UPN equals Email-field in Benemen Directory
  $adUser = Get-ADUser -Filter "UserPrincipalName -eq '$($d.Email)'" -Properties mobilePhone
  if ($adUser) { # user found from AD
    if ($aduser.MobilePhone -ne $d.MobilePhone) {
      # MobilePhone in AD is different than in Benemen Directory, update it
      Set-ADUser -Identity $adUser.SamAccountName -MobilePhone $d.MobilePhone
    }
  }
}
```