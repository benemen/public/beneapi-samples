

<#
.SYNOPSIS
Export Benemen Directory to CSV or Excel file

.DESCRIPTION
Export Benemen Directory to file, by calling BeneAPI REST service, and exporting results to file.
Full BeneAPI documentation can be found from https://doc.enreachvoice.com/beneapi/
API account with porper permissions are required.

Exporting to excel file requires ImportExcel module https://www.powershellgallery.com/packages/ImportExcel
CSV format use ";" as delimiter and UTF8 encoding

.PARAMETER APIUsername
Username to access BeneAPI, usually in form of email

.PARAMETER APIKey
API secret key to access BeneAPI

.PARAMETER EntryType
Which entrytypes should be exported. Options Users, DirectoryEntries, ServicePools, All. Default = All
Users = BeneCloud users 
DirectoryEntries = only informatiomnal directory entries, ie. non-users
ServicePools = service pools
All = Get all types

.PARAMETER ExportType
How Directory entires should be exported. Options default, CSV, Excel
No ExportType given = Return as PowerShell object
CSV = Write to CSV file
Excel = Write to Excel file

.PARAMETER Directory
Name of directory to be exported. By default only one directory "Default" is created, which contains all users, service pools and directorye entries

.PARAMETER ExportPath
Optional filepath where export file will be saved. If not given, file is created on the current directory and 
filename is in format <directoryname>-directory-<date in format yyyy-MM-dd_HHmmss>.[csv|xlsx]

.PARAMETER NoMultiLines
Should remove line endings from fields that allow multiline text, like description field.


.EXAMPLE
Export Directory information of all types, returns directory as PowerShell object
Export-BeneDirectory -APIUsername api.user@example.com -APIKey secretkey

.EXAMPLE
Export Directory information of Users to Excel file
Export-BeneDirectory -APIUsername api.user@example.com -APIKey secretkey -EntryType DirectoryEntries -ExportType Excel 

.EXAMPLE
Export Directpry information of Directory entries to excel file
Export-BeneDirectory -APIUsername api.user@example.com -APIKey secretkey -ExportPath "C:\Exports\directory.xlsx" -ExportType Excel -EntryType DirectoryEntries

#>

[CmdletBinding()]
Param(
    [Parameter(Mandatory=$true)]
    [String]$APIUsername,

    [Parameter(Mandatory=$true)]
    [String]$APIKey,

    [ValidateSet("Users","DirectoryEntries","ServicePools","All")]
    [String]$EntryType = "All",

    [String]$Directory = "Default",

    [ValidateSet("CSV","Excel")]
    [String]$ExportType,

    [String]$ExportPath,  

    [Switch]$NoMultiLines
)

# force to use TLS1.2
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# get api endpoint from discoveryservice https://doc.enreachvoice.com/beneapi/#service-discovery
$discoveryInfo = Invoke-RestMethod "https://discover.beneservices.com/api/user/?user=$APIUserName"
if ($discoveryInfo) {
    $APIUrl = $discoveryInfo.apiEndpoint
    Write-Verbose "Retrieved apiendpoint: [$APIUrl]"
}
else {
    Write-Warning "No API endpoint found for user [$APIUsername]"
    break
}

# Create headers for basic authentication 
$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $APIUsername,$APIKey)))
$headers = @{
    Authorization = "Basic $base64AuthInfo"
    Accept = "application/json"
} 

try {
    # Get directories
    $directoryUrl = $APIUrl + "directory/"
    $dirs = Invoke-RestMethod -Uri $directoryUrl -Method Get -Headers $headers

    foreach($d in $dirs)
    {        
        if ($d.Name -eq $Directory)
        {
            #Paging parameters
            $MaxCount = 100 # How many item to get at one call
            $PageNum = 0 # Page number
            $ResultCount = $MaxCount 

            $filteredlist = @() # List for storing entries

            while ($ResultCount -eq $MaxCount)
            {
                Write-Verbose "Getting page $PageNum"

                $directoryEntriesUrl = $APIUrl + "directory/" + $d.Id + "/?MaxCount=$MaxCount&Page=$PageNum"
                $entries = Invoke-RestMethod -Uri $directoryEntriesUrl -Method Get -Headers $headers
                
                $ResultCount = $entries.Entries.Count
                Write-Verbose "ResultCount: $ResultCount"
                $PageNum++
    
                #Filter entry list
                switch ($EntryType) {
                    "Users"            { $filteredlist += $entries.Entries | Where-Object EntryType -eq 1 }
                    "ServicePools"     { $filteredlist += $entries.Entries | Where-Object EntryType -eq 2 }
                    "DirectoryEntries" { $filteredlist += $entries.Entries | Where-Object EntryType -eq 3 }
                    Default            { $filteredlist += $entries.Entries }
                }
            }

            Write-Verbose "All fetched, total count $($filteredlist.Count)"
            # get only relevant attributes, modify this if needed
            $entrylist = $filteredlist | Select-Object Id,Company,LastName,FirstName,Email,MobileNumber,WorkNumber,OtherNumber,Address,Postalcode,City,Country,Title,Department,Location,Subcompany,Group,Team,Superior,Substitute,Description,EntryType,ExternalId,Modified
             
            #remove linebrakes from description field if wanted
            if ($NoMultiLines)
            {
                foreach ($e in $entrylist)
                {
                    if ($e.Description)
                    {
                        $e.Description = $e.Description -replace "`n",""
                    }
                }
            }

            #Export to file    

            if ($ExportType -eq "CSV")
            {
                #Export csv
                if (-not $ExportPath)
                {
                    $ExportPath =  $entries.Name + "-directory-" + (Get-Date -Format "yyyy-MM-dd_HHmmss") +".csv"
                }
                $entrylist | Export-Csv -Path $ExportPath -Delimiter ";" -NoTypeInformation -Encoding UTF8
                Write-Host "Exported to $ExportPath"
            }
            elseif ($ExportType -eq "Excel")
            {
                #export excel
                if (-not $ExportPath)
                {
                    $ExportPath =  $entries.Name + "-directory-" + (Get-Date -Format "yyyy-MM-dd_HHmmss") + ".xlsx"
                }
                $entrylist | Export-Excel -Path $ExportPath -NoNumberConversion @("WorkNumber","MobileNumber","OtherNumber") -AutoSize -BoldTopRow
                Write-Host "Exported to $ExportPath"
            }
            else {
                Write-output $entrylist                
            }
        }
    }
}
catch {
    Write-Error $_
}