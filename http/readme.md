# Example API request

This folder contains exmple requests for common API usage scenarios. 

Examples are meant to be used with Visual Studio Code and it's excellent [REST client extension](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

API documentation can be found from https://doc.enreachvoice.com/beneapi

# Important things regarding JSON serialization

API supports two JSON flavors. These are referred to as v1 ("Legacy Microsoft") and v2 (modern cross-compatible) formats.

The client requests a specific JSON version by specifying HTTP header ```X-Json-Serializer```. If the header is missing, v1 will be used.

The differences between the two versions relate to how different data types are handled. The differences are listed below. Especially default serialization format of datetimes cause problems for many libraries, and rrequires manual offset handling to get right.


| Type	|   1   | 2     |
|-------|-------|-------|
|Datetime	| ASP.NET JSON datetime with offset  ```/Date(1422850592270+0300)/``` |	ISO8601 ```2015-02-02T07:16:32.270Z``` |
|Dictionary<string, string> |	List of Key-Value pairs [{ "Key":"Some key", "Value":"Some value" }] |	JSON object{ "Some key": "Some value" } |
TimeSpan | ISO8601 duration ```PT1M20.047S``` |	Time string ```00:01:20.0470000``` |

More details in [API documentations](https://doc.enreachvoice.com/beneapi/#serialization)

## Serialization examples


### Example: Servicecall

Request (default serialization)
``` http
GET /servicecall/100c3231-76bb-4759-9691-a76ffe0e6a57/ HTTP/1.1
Accept: application/json
```

Response (default serialization)
``` json
{
  "ANum": "+358409220163",
  "AnswerQueueId": "89c87284-83fe-e611-80c9-00505689257e",
  "AnswerQueueName": "Sales Service",
  "AnswerTime": "\/Date(1648454360703+0300)\/",
  "AnsweringUserId": "94c957dc-0051-e711-80c9-00505689257e",
  "AnsweringUserName": "Parsons Adam",
  "BNum": "+358409220158",
  "CallDuration": null,
  "DisconnectTime": "\/Date(1648454385727+0300)\/",
  "EntryQueueId": "89c87284-83fe-e611-80c9-00505689257e",
  "EntryQueueName": "Sales Service",
  "Id": "100c3231-76bb-4759-9691-a76ffe0e6a57",
  "LastEventTime": "\/Date(1648454385727+0300)\/",
  "LastQueueId": "89c87284-83fe-e611-80c9-00505689257e",
  "LastQueueName": "Sales Service",
  "OrganizationId": "29f6e823-b484-e611-80c9-00505689257e",
  "Result": 0,
  "Timestamp": "\/Date(1648454355187+0300)\/",
  "WaitTime": "PT5.516S"
}
```` 


Request (v2 serialization)
Request 
``` http
GET /servicecall/100c3231-76bb-4759-9691-a76ffe0e6a57/ HTTP/1.1
Accept: application/json
X-Json-Serializer: 2
```

Response (v2 serialization)
Request 
``` json
{
  "Id": "100c3231-76bb-4759-9691-a76ffe0e6a57",
  "OrganizationId": "29f6e823-b484-e611-80c9-00505689257e",
  "Timestamp": "2022-03-28T10:59:15.187Z",
  "EntryQueueId": "89c87284-83fe-e611-80c9-00505689257e",
  "EntryQueueName": "Sales Service",
  "AnswerQueueId": "89c87284-83fe-e611-80c9-00505689257e",
  "AnswerQueueName": "Sales Service",
  "LastQueueId": "89c87284-83fe-e611-80c9-00505689257e",
  "LastQueueName": "Sales Service",
  "ANum": "+358409220163",
  "BNum": "+358409220158",
  "Result": 0,
  "AnsweringUserId": "94c957dc-0051-e711-80c9-00505689257e",
  "AnsweringUserName": "Parsons Adam",
  "AnswerTime": "2022-03-28T10:59:20.703Z",
  "DisconnectTime": "2022-03-28T10:59:45.727Z",
  "LastEventTime": "2022-03-28T10:59:45.727Z",
  "WaitTime": "00:00:05.5160000",
  "CallDuration": null
}
```

### Example: Directory entry

Request (default serialization)
```http
GET /directory/2af6e823-b484-e611-80c9-00505689257e/entries/95c957dc-0051-e711-80c9-00505689257e/ HTTP/1.1
Accept: application/json
X-Json-Serializer: 2
```
Response (default serialization)

``` json
{
  "Address": "",
  "Availabilities": null,
  "City": "",
  "Company": "Example Inc.",
  "Country": "",
  "CustomFields": [
    {
      "Key": "EmployeeNumber",
      "Value": "123123"
    },
    {
      "Key": "Favourite Animal",
      "Value": "Yak"
    }
  ],
  "Department": "Support",
  "Description": "",
  "DirectoryId": "2af6e823-b484-e611-80c9-00505689257e",
  "Email": "adam.parsons@tst.beneservices.com",
  "EntryType": 1,
  "ExternalId": null,
  "FirstName": "Parsons",
  "Group": "",
  "Id": "95c957dc-0051-e711-80c9-00505689257e",
  "LastName": "Adam",
  "Location": "London",
  "MobileNumber": "+358409220164",
  "Modified": "\/Date(1669211852177+0000)\/",
  "OtherNumber": "",
  "PhoneticName": "",
  "PostalCode": "",
  "ProfileImageUrl": "https:\/\/tstcdn.beneservices.com\/QASSilo01\/profileimages\/94c957dc0051e71180c900505689257e_210427670.jpg",
  "QueueId": null,
  "Score": null,
  "Subcompany": "",
  "Substitute": "",
  "Superior": "",
  "Team": "",
  "Title": "Support Manager",
  "UserId": "94c957dc-0051-e711-80c9-00505689257e",
  "WorkNumber": null
}
```

Request (v2 serialization)
```http
GET /directory/2af6e823-b484-e611-80c9-00505689257e/entries/95c957dc-0051-e711-80c9-00505689257e/ HTTP/1.1
Accept: application/json
X-Json-Serializer: 2
```
Response (v2 serialization)
``` json
{
  "Id": "95c957dc-0051-e711-80c9-00505689257e",
  "DirectoryId": "2af6e823-b484-e611-80c9-00505689257e",
  "EntryType": 1,
  "UserId": "94c957dc-0051-e711-80c9-00505689257e",
  "QueueId": null,
  "ExternalId": null,
  "Email": "adam.parsons@tst.beneservices.com",
  "FirstName": "Parsons",
  "LastName": "Adam",
  "Description": "",
  "Title": "Support Manager",
  "WorkNumber": null,
  "MobileNumber": "+358409220164",
  "OtherNumber": "",
  "Company": "Example Inc.",
  "Subcompany": "",
  "Location": "London",
  "Department": "Support",
  "Group": "",
  "Team": "",
  "Superior": "",
  "Substitute": "",
  "Address": "",
  "PostalCode": "",
  "City": "",
  "Country": "",
  "ProfileImageUrl": "https://tstcdn.beneservices.com/QASSilo01/profileimages/94c957dc0051e71180c900505689257e_210427670.jpg",
  "PhoneticName": "",
  "Modified": "2022-11-23T13:57:32.177Z",
  "Availabilities": null,
  "CustomFields": {
    "EmployeeNumber": "123123",
    "Favourite Animal": "Yak"
  },
  "Score": null
}
```
